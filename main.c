#include <stdio.h>
#include <stdlib.h>
#include <time.h>

char hasard() {
    int random_num = rand() % 3;
    if (random_num == 0) {
        return 'R';
    } else if (random_num == 1) {
        return 'P';
    } else {
        return 'C';
    }
}

int comparaison(char choix_joueur, char choix_ordi) {
    if (choix_joueur == choix_ordi) {
        return 0;
    } else if ((choix_joueur == 'R' && choix_ordi == 'C') ||
               (choix_joueur == 'P' && choix_ordi == 'R') ||
               (choix_joueur == 'C' && choix_ordi == 'P')) {
        return 1;
    } else {
        return -1;
    }
}

int main() {
    char choix_joueur, choix_ordi;
    int resultat;

    srand(time(NULL));

    printf("Choisissez R pour Roche, P pour Papier, ou C pour Ciseaux : ");
    scanf(" %c", &choix_joueur);

    if (choix_joueur != 'R' && choix_joueur != 'P' && choix_joueur != 'C') {
        printf("Choix invalide. Utilisez R, P ou C.\n");
        return 1;
    }

    choix_ordi = hasard();
    printf("L'ordinateur a choisi %c.\n", choix_ordi);

    resultat = comparaison(choix_joueur, choix_ordi);

    if (resultat == 0) {
        printf("�galit� !\n");
    } else if (resultat == 1) {
        printf("Vous avez gagn� !\n");
    } else {
        printf("L'ordinateur a gagn� !\n");
    }

    return 0;
}

